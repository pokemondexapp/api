import com.google.gson.Gson
import di.appModule
import extentions.replaceUrl
import network.PokeApiService
import network.model.NamedAPIResource
import org.koin.core.context.startKoin
import utils.Languages
import utils.PokemonUtils
import java.io.File
import java.io.FileOutputStream


suspend fun main() {

    startKoin {
        modules(appModule)
    }

    val pokeApiService = Application().pokeApiService
    val pokemonList = pokeApiService.getList(offset = 0, limit = 3).results

//    setPokemonList(pokemonList, pokeApiService)
    pokemonList?.forEach {
        setPokemonDetail(pokeApiService, it.url?.replaceUrl("pokemon") ?: "0")
    }
}

private suspend fun setPokemonList(
    pokemonList: List<NamedAPIResource>?, pokeApiService: PokeApiService
) {
    Languages.values().forEach {
        printJson(
            pathName = "pokemon",
            element = PokemonUtils.getPokemonList(pokemonList, pokeApiService, it.value),
            country = it.value
        )
    }
}

private suspend fun setPokemonDetail(
    pokeApiService: PokeApiService, pokemonId: String
) {
    Languages.values().forEach {
        printJson(
            parentPath = "pokemon",
            pathName = pokemonId,
            element = PokemonUtils.getPokemonDetail(pokeApiService, pokemonId, it.value),
            country = it.value
        )
    }
}

private fun printJson(parentPath: String? = null, pathName: String, element: Any, country: String) {
    val json: String = Gson().toJson(element)

    val version = File("api", "v1")
    if (!version.exists()) version.mkdir()

    val location = File(version, country)
    if (!location.exists()) location.mkdir()

    val file = if (parentPath != null) {
        val parent = File(location, parentPath)
        if (!parent.exists()) parent.mkdir()
        File(parent, "$pathName.json")
    } else {
        File(location, "$pathName.json")
    }

    FileOutputStream(file).bufferedWriter().use { out ->
        out.write(json)
    }
}
