import network.PokeApiService
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import utils.MovesUtils
import utils.PokemonUtils

class Component : KoinComponent {

    val pokeApiService by inject<PokeApiService>()

    val pokemonUtils by inject<PokemonUtils>()

    val movesUtils by inject<MovesUtils>()

}