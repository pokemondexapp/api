package di

import network.Client
import network.PokeApiService
import org.koin.dsl.module
import utils.PokemonUtils

val appModule = module {

    single { Client() }

    single { PokeApiService(get()) }

    single { PokemonUtils(get()) }
}