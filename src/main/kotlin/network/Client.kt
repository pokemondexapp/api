package network

import kotlinx.coroutines.suspendCancellableCoroutine
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.Reader
import java.net.HttpURLConnection
import java.net.URL
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

class Client {

    suspend fun get(endPoint: String): String {
        val url = URL("https://pokeapi.co/api/v2/$endPoint")
        println("Request: $url")
        return request(url, "GET")
    }

    private suspend fun request(url: URL, method: String): String {
        return suspendCancellableCoroutine { continuation ->
            try {
                val reader: BufferedReader

                val connection: HttpURLConnection = url.openConnection() as HttpURLConnection
                with(connection) {
                    setRequestProperty("User-Agent", "Mozilla/5.0")
                    requestMethod = method
                    reader = BufferedReader(InputStreamReader(inputStream) as Reader)

                    val response = StringBuffer()

                    var inputLine = reader.readLine()
                    while (inputLine != null) {
                        response.append(inputLine)
                        inputLine = reader.readLine()
                    }
                    reader.close()

                    if (continuation.isActive) {
                        continuation.resume(response.toString())
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
                if (continuation.isActive) {
                    continuation.resumeWithException(e)
                }
            }
        }
    }

}