package network

import com.google.gson.Gson
import network.model.*
import readFile

class PokeApiService(private val client: Client) {

    suspend fun getPokemonList(offset: Int = 0, limit: Int = 898): PokemonApiList {
        return Gson().fromJson(client.get("pokemon?offset=$offset&limit=$limit"), PokemonApiList::class.java)
    }

    suspend fun getTypeList(): PokemonApiList {
        return Gson().fromJson(client.get("type"), PokemonApiList::class.java)
    }

    suspend fun getPokemon(id: String): PokeApiPokemon {
        return Gson().fromJson(client.get("pokemon/$id"), PokeApiPokemon::class.java)
    }

    suspend fun getPokemonSpecies(id: String): PokeApiSpecies {
        return Gson().fromJson(client.get("pokemon-species/$id"), PokeApiSpecies::class.java)
    }

    suspend fun getPokemonForm(id: String): PokeApiForms {
        return Gson().fromJson(client.get("pokemon-form/$id"), PokeApiForms::class.java)
    }

    suspend fun getType(id: String): PokeApiType {
        return Gson().fromJson(client.get("type/$id"), PokeApiType::class.java)
    }

    fun getTypeLocal(id: String, country: String): PokemonType {
        return Gson().fromJson(readFile(country, "type", id), PokemonType::class.java)
    }

    suspend fun getAbility(id: String): PokeApiAbility {
        return Gson().fromJson(client.get("ability/$id"), PokeApiAbility::class.java)
    }

    suspend fun getEggGroup(id: String): PokeApiEggGroup {
        return Gson().fromJson(client.get("egg-group/$id"), PokeApiEggGroup::class.java)
    }

    suspend fun getMove(id: String): PokeApiMove {
        return Gson().fromJson(client.get("move/$id"), PokeApiMove::class.java)
    }

    suspend fun getMachine(id: String): PokeApiMachine {
        return Gson().fromJson(client.get("machine/$id"), PokeApiMachine::class.java)
    }

    suspend fun getItem(id: String): PokeApiItems {
        return Gson().fromJson(client.get("item/$id"), PokeApiItems::class.java)
    }

    suspend fun getEvolutionChain(id: String): PokeApiEvolutionChain {
        return Gson().fromJson(client.get("evolution-chain/$id"), PokeApiEvolutionChain::class.java)
    }

}