package network.model

data class PokeApiMachine(
    val id: Int? = null, val item: NamedAPIResource? = null, val move: NamedAPIResource? = null
)
