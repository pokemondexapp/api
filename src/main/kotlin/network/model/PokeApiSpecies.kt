package network.model

import utils.MapLocalize

data class PokeApiSpecies(
    val id: Int? = null,
    val capture_rate: Long? = null,
    val color: NamedAPIResource? = null,
    val evolution_chain: NamedAPIResource? = null,
    val flavor_text_entries: List<FlavorTextEntry>? = null,
    val gender_rate: Int? = null,
    val genera: List<Genus>? = null,
    val generation: NamedAPIResource? = null,
    val has_gender_differences: Boolean? = null,
    val hatch_counter: Long? = null,
    val is_baby: Boolean? = null,
    val is_legendary: Boolean? = null,
    val is_mythical: Boolean? = null,
    val name: String? = null,
    val names: List<PokeAPiName>? = null,
    val pokedex_numbers: List<PokedexNumber>? = null,
    val varieties: List<Variety>? = null,
    val egg_groups: List<NamedAPIResource>? = null
)

data class FlavorTextEntry(
    val flavor_text: String? = null,
    val language: NamedAPIResource? = null,
    val version: NamedAPIResource? = null
)

data class Genus(
    val genus: String? = null,
    val language: NamedAPIResource? = null
)

data class PokeAPiName(
    val language: NamedAPIResource? = null,
    val name: String? = null
)

data class PokedexNumber(
    val entry_number: Long? = null,
    val pokedex: NamedAPIResource? = null
)

data class Variety(
    val is_default: Boolean? = null,
    val pokemon: NamedAPIResource? = null
)

fun PokeApiSpecies.localize(
    localize: MapLocalize, country: String
): String {
    return when (localize) {
        MapLocalize.NAME -> this.getName(country).replace("\n", " ").trim()
        MapLocalize.DESCRIPTION -> this.getDescription(country).replace("\n", " ").trim()
        MapLocalize.GENUS -> this.getGenus(country).replace("\n", " ").trim()
        else -> ""
    }
}

private fun PokeApiSpecies.getName(country: String): String {
    return this.names?.firstOrNull { it.language?.name == country }?.name ?: ""
}

private fun PokeApiSpecies.getDescription(country: String): String {
    return this.flavor_text_entries?.firstOrNull { it.language?.name == country }?.flavor_text ?: ""
}

private fun PokeApiSpecies.getGenus(country: String): String {
    return this.genera?.firstOrNull { it.language?.name == country }?.genus ?: ""
}
