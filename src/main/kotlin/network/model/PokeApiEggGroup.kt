package network.model

data class PokeApiEggGroup(
    val id: Long? = null,
    val name: String? = null,
    val pokemon_species: List<NamedAPIResource>? = null
) : PokeApi()

data class Name(
    val language: NamedAPIResource? = null,
    val name: String? = null
)
