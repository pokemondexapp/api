package network.model

data class PokemonDexList(
    val id: Int? = null,
    val number: String? = null,
    val name: String? = null,
    val sprite: String? = null,
    val type1: String? = null,
    val type2: String? = null
)

data class PokemonDetail(
    val id: Int? = null,
    val name: String? = null,
    val number: String? = null,
    val generation: Int? = null,
    val genus: String? = null,
    val description: String? = null,
    val height: Double? = null,
    val weight: Double? = null,
    val isBaby: Boolean? = null,
    val isMythical: Boolean? = null,
    val isLegendary: Boolean? = null,
    val genderRate: String? = null,
    val captureRate: Long? = null,
    val type1: String? = null,
    val type2: String? = null,
    val stats: Map<String, Int>? = null,
    val abilities: List<PokemonAbilities>? = null,
    val eggGroups: List<String>? = null,
    val evolutions: List<PokemonEvolutions>? = null,
    val megaEvolutions: List<PokemonEvolutions>? = null,
    val form: List<PokemonDexList>? = null,
    val weakness: List<PokemonWeakness>? = null
)

data class PokemonType(
    val id: Int? = null,
    val damage: TypeDamage? = null,
    val name: String? = null,
)

data class PokemonWeakness(
    val damage_x4: List<String>? = null,
    val damage_x2: List<String>? = null,
    val damage_x1: List<String>? = null,
    val damage_x0_5: List<String>? = null,
    val damage_x0_25: List<String>? = null,
    val damage_x0: List<String>? = null
)

data class TypeDamage(
    val doubleDamage: List<String>? = null,
    val normalDamage: List<String>? = null,
    val halfDamage: List<String>? = null,
    val noDamage: List<String>? = null
)

data class PokemonAbilities(var isHidden: Boolean, var name: String, var description: String?)

data class PokemonMoves(
    val move: String? = null,
    val moveDetail: List<MoveDetail>? = null
)

data class MoveDetail(
    val levelLearned: Long? = null,
    val moveLearnMethod: String? = null,
    val versionGroup: String? = null
)

data class PokemonEvolutions(
    var id: Int? = null,
    var stadium: Int? = null,
    var itemImage: String? = null,
    var trigger: String? = null,
    var number: String? = null,
    var name: String? = null,
    var sprite: String? = null,
    var type1: String? = null,
    var type2: String? = null
)

data class PokemonDexType(
    val id: String? = null,
    val name: String? = null,
    val dark: Double? = null,
    val ice: Double? = null,
    val ghost: Double? = null,
    val flying: Double? = null,
    val normal: Double? = null,
    val dragon: Double? = null,
    val fighting: Double? = null,
    val psychic: Double? = null,
    val electric: Double? = null,
    val fire: Double? = null,
    val steel: Double? = null,
    val ground: Double? = null,
    val bug: Double? = null,
    val fairy: Double? = null,
    val grass: Double? = null,
    val poison: Double? = null,
    val water: Double? = null,
    val rock: Double? = null
)


data class FirebaseMoves(
    val id: String? = null,
    val names: String? = null,
    val description: String? = null,
    val accuracy: Int? = null,
    val damage_class: String? = null,
    val power: Int? = null,
    val pp: Int? = null,
    val priority: Int? = null,
    val type: String? = null
)

data class FirebaseMachine(
    val id: String? = null,
    val item: String? = null,
    val move: String? = null,
    val description: String? = null,
    val names: String? = null,
    val sprites: String? = null
)
