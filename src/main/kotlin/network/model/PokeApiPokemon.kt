package network.model

import kotlinx.serialization.SerialName
import utils.MapLocalize

data class PokeApiPokemon(
    val id: Int? = null,
    val abilities: List<PokeApiAbilities>? = null,
    val height: Int? = null,
    val forms: List<NamedAPIResource>? = null,
    val moves: List<PokeApiMoves>? = null,
    val name: String? = null,
    val species: NamedAPIResource? = null,
    val sprites: PokeApiSprites? = null,
    val stats: List<PokeApiStats>? = null,
    val types: List<PokeApiTypes>? = null,
    val weight: Int? = null
)

data class PokeApiAbilities(
    val ability: NamedAPIResource? = null, val is_hidden: Boolean? = null, val slot: Long? = null
)

data class PokeApiMoves(
    val move: NamedAPIResource? = null, val version_group_details: List<VersionGroupDetail>? = null
)

data class VersionGroupDetail(
    val level_learned_at: Long? = null,
    val move_learn_method: NamedAPIResource? = null,
    val version_group: NamedAPIResource? = null
)

data class PokeApiSprites(
    val default: String? = null,
    val back_default: String? = null,
    val back_shiny: String? = null,
    val front_default: String? = null,
    val front_shiny: String? = null,
    val other: OtherSprites? = null
)

data class OtherSprites(
    @SerialName("official-artwork") val officialArtwork: OfficialArtwork? = null
)

data class OfficialArtwork(
    val front_default: String? = null
)

data class PokeApiStats(
    val base_stat: Long? = null, val effort: Long? = null, val stat: NamedAPIResource? = null
)

data class PokeApiTypes(
    val slot: Long? = null, val type: NamedAPIResource? = null
)

data class NamedAPIResource(
    val name: String? = null, val url: String? = null
)

data class APIResource(
    val url: String? = null
)

fun PokeApiPokemon.localize(
    localize: MapLocalize, country: String
): String {
    return when (localize) {
        MapLocalize.FORM -> this.getForm(country).trim()
        else -> ""
    }
}

private fun PokeApiPokemon.getForm(country: String): String {
    return this.forms?.firstOrNull { it.name == country }?.name ?: ""
}