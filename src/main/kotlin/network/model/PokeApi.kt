package network.model

import utils.MapLocalize

data class PokemonApiList(
    val count: Int? = null,
    val next: String? = null,
    val previous: String? = null,
    val results: List<NamedAPIResource>? = null
)

open class PokeApi(
    val names: List<Name>? = null, val flavor_text_entries: List<ItemDescription>? = null
)

fun PokeApi.localize(
    localize: MapLocalize, country: String
): String {
    return when (localize) {
        MapLocalize.NAME -> this.getName(country).replace("\n", " ").trim()
        MapLocalize.DESCRIPTION -> this.getDescription(country).replace("\n", " ").trim()
        else -> ""
    }
}

private fun PokeApi.getName(country: String): String {
    return this.names?.lastOrNull { it.language?.name == country }?.name ?: ""
}

private fun PokeApi.getDescription(country: String): String {
    return this.flavor_text_entries?.lastOrNull { it.language?.name == country }?.text ?: ""
}
