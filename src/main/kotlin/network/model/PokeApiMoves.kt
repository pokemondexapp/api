package network.model

data class PokeApiMove(
    val id: Long? = null,
    val accuracy: Long? = null,
    val damage_class: NamedAPIResource? = null,
    val machines: List<PokeApiMachines>? = null,
    val name: String? = null,
    val power: Long? = null,
    val pp: Long? = null,
    val priority: Long? = null,
    val type: NamedAPIResource? = null
) : PokeApi()

data class PokeApiMachines(
    val machine: APIResource? = null, val version_group: NamedAPIResource? = null
)
