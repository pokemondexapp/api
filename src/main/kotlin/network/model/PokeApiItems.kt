package network.model

data class PokeApiItems(
    val attributes: List<NamedAPIResource>? = null,
    val baby_trigger_for: APIResource? = null,
    val category: NamedAPIResource? = null,
    val cost: Int? = null,
    val effect_entries: List<VerboseEffect>? = null,
    val fling_effect: NamedAPIResource? = null,
    val fling_power: Int? = null,
    val game_indices: List<GameIndex>? = null,
    val id: Long? = null,
    val machines: List<PokeApiMachines>? = null,
    val name: String? = null,
    val sprites: PokeApiSprites? = null
): PokeApi()

data class ItemDescription(
    val text: String? = null,
    val language: NamedAPIResource? = null,
    val version: NamedAPIResource? = null
)

data class VerboseEffect(
    val effect: String? = null,
    val short_effect: String? = null,
    val language: NamedAPIResource? = null
)
