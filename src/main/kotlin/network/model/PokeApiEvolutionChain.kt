package network.model

data class PokeApiEvolutionChain(
    val id: Long? = null,
    val baby_trigger_item: NamedAPIResource? = null,
    val chain: Chain? = null
)

data class Chain(
    val evolution_details: List<EvolutionDetail>? = null,
    val evolves_to: List<Chain>? = null,
    val is_baby: Boolean? = null,
    val species: NamedAPIResource? = null
)

// TODO: see https://wiki.pokemoncentral.it/Eevee#Evoluzioni to map evolution
// Add new param to special
data class EvolutionDetail(
    val gender: Int? = null,
    val held_item: NamedAPIResource? = null,
    val item: NamedAPIResource? = null,
    val known_move: NamedAPIResource? = null,
    val known_move_type: NamedAPIResource? = null,
    val location: NamedAPIResource? = null, // Magnemite, Eevee, Nosepass
    val min_affection: Int? = null, // Eevee //TODO add in other
    val min_beauty: Int? = null, // 171, 170 (Feebas, Milotic) //TODO add in other
    val min_happiness: Int? = null, // sempre 220 (Felicità) //TODO add in other
    val min_level: Int? = null,
    val party_species: NamedAPIResource? = null, // (Mantyke, Mantine) Aumento di livello con Remoraid in squadra //TODO add in other
    val relative_physical_stats: Int? = null, // 1, -1, 0 (Tyrogue evolution) 1 means Attack > Defense. 0 means Attack = Defense. -1 means Attack < Defense. //TODO add in other
    val time_of_day: String? = null, // night, day
    val trigger: NamedAPIResource? = null, // Accept only level-up, trade, use-item //TODO shed (Shedinja), other (Approfondire) see https://pokeapi.co/api/v2/evolution-trigger/{id}
)
