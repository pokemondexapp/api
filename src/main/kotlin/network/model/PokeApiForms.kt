package network.model

data class PokeApiForms(
    val form_name: String? = null,
    val form_names: List<PokeAPiName>? = null,
    val form_order: Long? = null,
    val id: Long? = null,
    val is_battle_only: Boolean? = null,
    val is_default: Boolean? = null,
    val is_mega: Boolean? = null,
    val name: String? = null,
    val names: List<PokeAPiName>? = null,
    val order: Long? = null,
    val pokemon: NamedAPIResource? = null,
    val sprites: PokeApiSprites? = null,
    val types: List<Type>? = null,
    val version_group: NamedAPIResource? = null
)

data class Type(
    val slot: Long? = null,
    val type: NamedAPIResource? = null
)
