package network.model

data class PokeApiAbility(
    val id: Long? = null,
    val effect_changes: List<EffectChange>? = null,
    val effect_entries: List<PokemonAbilitiesEffectEntry>? = null,
    val generation: NamedAPIResource? = null,
    val is_main_series: Boolean? = null,
    val name: String? = null,
    val pokemon: List<PokemonWithAbility>? = null
) : PokeApi()

data class EffectChange(
    val effect_entries: List<EffectChangeEffectEntry>? = null, val version_group: NamedAPIResource? = null
)

data class EffectChangeEffectEntry(
    val effect: String? = null, val language: NamedAPIResource? = null
)

data class PokemonAbilitiesEffectEntry(
    val effect: String? = null, val language: NamedAPIResource? = null, val short_effect: String? = null
)

data class PokemonWithAbility(
    val is_hidden: Boolean? = null, val pokemon: NamedAPIResource? = null, val slot: Long? = null
)
