package network.model

data class PokeApiType(
    val id: Int? = null,
    val damage_relations: DamageRelations? = null,
    val game_indices: List<GameIndex>? = null,
    val generation: NamedAPIResource? = null,
    val move_damage_class: NamedAPIResource? = null,
    val moves: List<NamedAPIResource>? = null,
    val name: String? = null,
    val pokemon: List<PokemonWithType>? = null
) : PokeApi()

data class DamageRelations(
    val double_damage_from: List<NamedAPIResource>? = null,
    val half_damage_from: List<NamedAPIResource>? = null,
    val double_damage_to: List<NamedAPIResource>? = null,
    val half_damage_to: List<NamedAPIResource>? = null,
    val no_damage_from: List<NamedAPIResource>? = null,
    val no_damage_to: List<NamedAPIResource>? = null
)

data class GameIndex(
    val game_index: Long? = null, val generation: NamedAPIResource? = null
)

data class PokemonWithType(
    val pokemon: NamedAPIResource? = null, val slot: Long? = null
)

data class TypeUiModel(
    val id: String? = null,
    val dark: Double? = null,
    val ice: Double? = null,
    val ghost: Double? = null,
    val flying: Double? = null,
    val normal: Double? = null,
    val dragon: Double? = null,
    val fighting: Double? = null,
    val psychic: Double? = null,
    val electric: Double? = null,
    val fire: Double? = null,
    val steel: Double? = null,
    val ground: Double? = null,
    val bug: Double? = null,
    val fairy: Double? = null,
    val grass: Double? = null,
    val poison: Double? = null,
    val water: Double? = null,
    val rock: Double? = null
)
