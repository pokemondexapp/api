package utils

enum class Languages(val value: String) {
    IT("it"),
    EN("en"),
    ES("es"),
    DE("de"),
    FR("fr"),
    JA("ja")
}