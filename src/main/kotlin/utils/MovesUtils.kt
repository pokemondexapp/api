package utils

import extentions.replaceUrl
import network.PokeApiService
import network.model.FirebaseMachine
import network.model.FirebaseMoves
import network.model.PokeApiItems
import network.model.localize

class MovesUtils(private val pokeApiService: PokeApiService) {

    suspend fun insertMoves(country: String): List<FirebaseMoves> {
        val list = mutableListOf<FirebaseMoves>()
        for (id in 1..826) { //826
            val moves = pokeApiService.getMove(id.toString())
            val movesId = "move_${moves.name?.replace("-", "_")}"
            val firebaseMoves = FirebaseMoves(
                id = movesId,
                names = moves.localize(MapLocalize.NAME, country),
                description = moves.localize(MapLocalize.DESCRIPTION, country),
                accuracy = moves.accuracy?.toInt(),
                damage_class = moves.damage_class?.name,
                power = moves.power?.toInt(),
                pp = moves.pp?.toInt(),
                priority = moves.priority?.toInt(),
                type = "type_${moves.type?.name?.replace("-", "_")}"
            )
            list.add(firebaseMoves)
        }
        return list
    }

    suspend fun insertMachine(country: String): List<FirebaseMachine> {
        val list = mutableListOf<FirebaseMachine>()
        for (id in 1..1442) {
            val moves = pokeApiService.getMachine(id.toString())

            val item: PokeApiItems = pokeApiService.getItem(moves.item?.url?.replaceUrl("item") ?: "")

            val movesId = "machine_${moves.move?.name?.replace("-", "_")}"
            val firebaseMachine = FirebaseMachine(
                id = movesId,
                item = moves.item?.name?.replace("-", "_"),
                move = moves.move?.name?.replace("-", "_"),
                names = item.localize(MapLocalize.NAME, country),
                description = item.localize(MapLocalize.DESCRIPTION, country),
                sprites = item.sprites?.default
            )
            list.add(firebaseMachine)
        }
        return list
    }

}
