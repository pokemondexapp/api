package utils

import extentions.pokemon.*
import extentions.replaceUrl
import network.PokeApiService
import network.model.*

class PokemonUtils(private val pokeApiService: PokeApiService) {

    suspend fun getPokemonList(
        pokemonList: List<NamedAPIResource>?, country: String, isFormName: Boolean = false
    ): List<PokemonDexList> {
        val pokemonDexList = mutableListOf<PokemonDexList>()
        pokemonList?.forEach { it ->

            val pokemon = getPokemon(it.name ?: "")
            val species = getPokemonSpecies(pokemon.id.toString(), pokemon)
            val pokemonForm = getPokemonForm(pokemon)
            val name = if (isFormName) pokemon.name?.getFormName(species, country)?.trim()
            else species.getName(country).trim()
            val sprite = if (isFormName) "${Constants.sprite}/pokemon/other/official-artwork/${pokemon.id}.png"
            else "${Constants.sprite}/pokemon/${pokemon.id}.png"

            val pokemonDex = PokemonDexList(
                id = pokemon.id,
                number = pokemon.getNumber(),
                name = name,
                sprite = sprite,
                type1 = getPokemonType(pokemon, 0, country)?.name,
                type2 = getPokemonType(pokemon, 1, country)?.name
            )
            if (isFormName) {
                if (pokemonForm.is_mega == false) {
                    pokemonDexList.add(pokemonDex)
                }
            } else {
                pokemonDexList.add(pokemonDex)
            }
        }
        return pokemonDexList
    }

    suspend fun getPokemonDetail(
        pokemonId: String, country: String
    ): PokemonDetail {

        val pokemon = getPokemon(pokemonId)
        val species = getPokemonSpecies(pokemonId, pokemon)
        val evolutionChain = getEvolutionChain(species)

        val genderRate = species.gender_rate?.toDouble() ?: 0.0
        val maleValue = (8 - genderRate) / 8
        val femaleValue = genderRate / 8

        return PokemonDetail(
            id = pokemon.id,
            name = pokemon.name?.getFormName(species, country)?.trim(),
            number = pokemon.getNumber(),
            description = species.localize(MapLocalize.DESCRIPTION, country),
            genus = species.localize(MapLocalize.GENUS, country),
            type1 = getPokemonType(pokemon, 0, country)?.name,
            type2 = getPokemonType(pokemon, 1, country)?.name,
            generation = species.getGeneration(),
            form = getPokemonList(species.getFormList(), country, true),
            evolutions = evolutionChain.getEvolutionList(pokeApiService, country),
            megaEvolutions = getPokemonMegaList(species.getFormList(), country),
            genderRate = if (genderRate == -1.0) "gender-less".localize(country) else "${(maleValue * 100)}% ♂ - ${(femaleValue * 100)}% ♀",
            captureRate = species.capture_rate,
            eggGroups = species.getEggGroups(pokeApiService, country),
            height = pokemon.height?.div(10.0),
            weight = pokemon.weight?.div(10.0),
            abilities = pokemon.getAbilities(pokeApiService, country),
            stats = pokemon.getStats(),
            isBaby = species.is_baby,
            isLegendary = species.is_legendary,
            isMythical = species.is_mythical,
//            weakness = getWeakness(pokemon.types.)
        )
    }

    private suspend fun getPokemonMegaList(
        pokemonList: List<NamedAPIResource>?, country: String
    ): List<PokemonEvolutions> {
        val pokemonDexList = mutableListOf<PokemonEvolutions>()
        pokemonList?.forEach { it ->

            val pokemon = getPokemon(it.name ?: "")
            val species = getPokemonSpecies(pokemon.id.toString(), pokemon)
            val pokemonForm = getPokemonForm(pokemon)

            val pokemonDex = PokemonEvolutions(
                id = pokemon.id,
                number = pokemon.getNumber(),
                name = pokemon.name?.getFormName(species, country)?.trim(),
                sprite = "${Constants.sprite}/pokemon/other/official-artwork/${pokemon.id}.png",
                type1 = getPokemonType(pokemon, 0, country)?.name,
                type2 = getPokemonType(pokemon, 1, country)?.name
            )

            if (pokemonForm.is_mega == true) {
                pokemonDexList.add(pokemonDex)
            }
        }
        return pokemonDexList
    }

    suspend fun getType(country: String): List<PokemonDexType> {
        val list = mutableListOf<PokemonDexType>()
        for (id in 1..18) {

            val type = pokeApiService.getType(id.toString())
            val pokemonType = PokemonDexType(
                id = "type_${type.name?.replace("-", "_")}",
                name = type.localize(MapLocalize.NAME, country),
                bug = type.getDamage("bug"),
                dark = type.getDamage("dark"),
                dragon = type.getDamage("dragon"),
                electric = type.getDamage("electric"),
                fairy = type.getDamage("fairy"),
                fighting = type.getDamage("fighting"),
                fire = type.getDamage("fire"),
                flying = type.getDamage("flying"),
                ghost = type.getDamage("ghost"),
                grass = type.getDamage("grass"),
                ground = type.getDamage("ground"),
                ice = type.getDamage("ice"),
                normal = type.getDamage("normal"),
                poison = type.getDamage("poison"),
                psychic = type.getDamage("psychic"),
                rock = type.getDamage("rock"),
                steel = type.getDamage("steel"),
                water = type.getDamage("water")
            )
            list.add(pokemonType)
        }
        return list
    }

    private fun PokeApiType.getDamage(type: String): Double {
        var damage = 1.0

        damage_relations?.double_damage_from?.forEach {
            if (it.name == type) {
                damage = 2.0
            }
        }

        damage_relations?.half_damage_from?.forEach {
            if (it.name == type) {
                damage = 0.5
            }
        }

        damage_relations?.no_damage_from?.forEach {
            if (it.name == type) {
                damage = 0.0
            }
        }
        return damage
    }

    private suspend fun getPokemon(pokemonId: String): PokeApiPokemon {
        return pokeApiService.getPokemon(pokemonId)
    }

    private suspend fun getPokemonSpecies(pokemonId: String, pokemon: PokeApiPokemon): PokeApiSpecies {
        return if (pokemonId.toInt() < 10_000) {
            pokeApiService.getPokemonSpecies(pokemonId)
        } else {
            val speciesId = pokemon.species?.url?.replaceUrl("pokemon-species") ?: "0"
            pokeApiService.getPokemonSpecies(speciesId)
        }
    }

    private fun getPokemonFormId(pokemon: PokeApiPokemon): String {
        return pokemon.forms?.first()?.url?.replaceUrl("pokemon-form") ?: ""
    }

    private suspend fun getPokemonForm(pokemon: PokeApiPokemon): PokeApiForms {
        return pokeApiService.getPokemonForm(getPokemonFormId(pokemon))
    }

    private fun getEvolutionChainId(species: PokeApiSpecies): String {
        return species.evolution_chain?.url?.replaceUrl("evolution-chain") ?: "0"
    }

    private suspend fun getEvolutionChain(species: PokeApiSpecies): PokeApiEvolutionChain {
        return pokeApiService.getEvolutionChain(getEvolutionChainId(species))
    }

    private suspend fun getMove(moveId: String): PokeApiMove {
        return pokeApiService.getMove(moveId)
    }

    private fun getPokemonType(pokemon: PokeApiPokemon, index: Int, country: String): PokemonType? {
        return if ((pokemon.types?.size ?: 0) > index) {
            val name = pokemon.types?.get(index)?.type?.name
            pokeApiService.getTypeLocal(name ?: "0", country)
        } else null
    }
}

enum class PokemonTypeEnum(val value: String) {
    BUG("type_bug"),
    DARK("type_dark"),
    DRAGON("type_dragon"),
    ELECTRIC("type_electric"),
    FAIRY("type_fairy"),
    FIGHTING("type_fighting"),
    FIRE("type_fire"),
    FLYING("type_flying"),
    GHOST("type_ghost"),
    GRASS("type_grass"),
    GROUND("type_ground"),
    ICE("type_ice"),
    NORMAL("type_normal"),
    POISON("type_poison"),
    PSYCHIC("type_psychic"),
    ROCK("type_rock"),
    STEEL("type_steel"),
    WATER("type_water"),
}