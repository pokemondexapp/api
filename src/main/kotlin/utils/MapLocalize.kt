package utils

enum class MapLocalize {
    NAME,
    DESCRIPTION,
    GENUS,
    FORM
}
