package utils

fun String.localize(country: String): String {
    return when(this) {
        "fire-stone" -> when (country) {
            Languages.FR.value -> "Pierre Feu"
            Languages.DE.value -> "Feuerstein"
            Languages.ES.value -> "Piedra Fuego"
            Languages.IT.value -> "Pietrafocaia"
            Languages.EN.value -> "Fire Stone"
            Languages.JA.value -> "ほのおのいし"
            else -> ""
        }
        "thunder-stone" -> when (country) {
            Languages.FR.value -> "Pierre Foudre"
            Languages.DE.value -> "Donnerstein"
            Languages.ES.value -> "Piedra Trueno"
            Languages.IT.value -> "Pietratuono"
            Languages.EN.value -> "Thunder Stone"
            Languages.JA.value -> "かみなりのいし"
            else -> ""
        }
        "water-stone" -> when (country) {
            Languages.FR.value -> "Pierre Eau"
            Languages.DE.value -> "Wasserstein"
            Languages.ES.value -> "Piedra Agua"
            Languages.IT.value -> "Pietraidrica"
            Languages.EN.value -> "Water Stone"
            Languages.JA.value -> "みずのいし"
            else -> ""
        }
        "leaf-stone" -> when (country) {
            Languages.FR.value -> "Pierre Plante"
            Languages.DE.value -> "Blattstein"
            Languages.ES.value -> "Piedra Hoja"
            Languages.IT.value -> "Pietrafoglia"
            Languages.EN.value -> "Leaf Stone"
            Languages.JA.value -> "リーフのいし"
            else -> ""
        }
        "ice-stone" -> when (country) {
            Languages.FR.value -> "Pierre Glace"
            Languages.DE.value -> "Eisstein"
            Languages.ES.value -> "Piedra Hielo"
            Languages.IT.value -> "Pietragelo"
            Languages.EN.value -> "Ice Stone"
            Languages.JA.value -> "こおりのいし"
            else -> ""
        }
        "happiness" -> when (country) {
            Languages.FR.value -> "Bonheur"
            Languages.DE.value -> "Level-Up mit einer Zuneigung"
            Languages.ES.value -> "Amistad"
            Languages.IT.value -> "Legame"
            Languages.EN.value -> "Friendship"
            Languages.JA.value -> "なついている状態でレベルアップで"
            else -> ""
        }
        "happiness-day" -> when (country) {
            Languages.FR.value -> "Bonheur Jour"
            Languages.DE.value -> "Level-Up am Tag"
            Languages.ES.value -> "Amistad + subir un nivel en el día"
            Languages.IT.value -> "Legame di giorno"
            Languages.EN.value -> "Friendship by day"
            Languages.JA.value -> "朝・昼・夕方になつき、XDではたいようのかけらを持っているときにレベルアップでエーフィ"
            else -> ""
        }
        "happiness-night" -> when (country) {
            Languages.FR.value -> "Bonheur Nuit"
            Languages.DE.value -> "Level-Up in der Nacht"
            Languages.ES.value -> "Amistad + subir un nivel en la noche"
            Languages.IT.value -> "Legame di notte"
            Languages.EN.value -> "Friendship by night"
            Languages.JA.value -> "夜・深夜になつき、XDではつきのかけらを持っているときにレベルアップでブラッキー"
            else -> ""
        }
        "level-affection-fairy" -> when (country) {
            Languages.FR.value -> "Gagner un niveau en ayant une capacité fée avec 2 cœurs d'affection"
            Languages.DE.value -> "Level-Up mit Fee-Attacke und einer Zuneigung von 220"
            Languages.ES.value -> "Conocer un movimiento hada + subir un nivel + dos corazones de afecto"
            Languages.IT.value -> "Aumento di livello con il legame moderatamente alto avendo appreso una mossa di tipo Folletto"
            Languages.EN.value -> "Level up with moderately high affection by learning a Fairy-type move"
            Languages.JA.value -> "なかよしレベルが2以上(第八世代ではなかよし度160以上)にしてフェアリータイプの技を覚えてレベルアップでニンフィア"
            else -> ""
        }
        "level-up" -> when (country) {
            Languages.FR.value -> "Niveau"
            Languages.DE.value -> "Level"
            Languages.ES.value -> "Nivel"
            Languages.IT.value -> "Livello"
            Languages.EN.value -> "Level up"
            Languages.JA.value -> "Lv."
            else -> ""
        }
        "trade" -> when (country) {
            Languages.FR.value -> "Échange"
            Languages.DE.value -> "Tausch"
            Languages.ES.value -> "Intercambio"
            Languages.IT.value -> "Scambio"
            Languages.EN.value -> "Trade"
            Languages.JA.value -> "通信交換で"
            else -> ""
        }
        "use-item" -> when (country) {
            Languages.FR.value -> "Utilisation d'un objet"
            Languages.DE.value -> "Gegenstand nutzen"
            Languages.ES.value -> "Utilizar artículo"
            Languages.IT.value -> "Usa elemento"
            Languages.EN.value -> "Use item"
            Languages.JA.value -> "アイテムを使う"
            else -> ""
        }
        "shed" -> when (country) {
            Languages.FR.value -> "Place dans l'équipe et une Poké Ball"
            Languages.DE.value -> "Platz im Team und ein Pokéball"
            Languages.ES.value -> "Hat Platz im Team und Pokébälle in der Tasche"
            Languages.IT.value -> "Con spazio in squadra e almeno una Poké Ball nella Borsa"
            Languages.EN.value -> "Space in team and at least pokeball in bag"
            Languages.JA.value -> "彼はチームに余裕があり、ポケットにポケットボールを持っています"
            else -> ""
        }
        "gender-less" -> when (country) {
            Languages.FR.value -> "Asexué"
            Languages.DE.value -> "Geschlechtslos"
            Languages.ES.value -> "Sin género"
            Languages.IT.value -> "Senza Genere"
            Languages.EN.value -> "Genderless"
            Languages.JA.value -> "ジェンダーレス"
            else -> ""
        }
        "id" -> when (country) {
            Languages.FR.value -> ""
            Languages.DE.value -> ""
            Languages.ES.value -> ""
            Languages.IT.value -> ""
            Languages.EN.value -> ""
            Languages.JA.value -> ""
            else -> ""
        }
        else -> ""
    }
}
