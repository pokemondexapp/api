package extentions

fun String.replaceUrl(endpoint: String): String {
    return this.replace("https://pokeapi.co/api/v2/$endpoint/", "").replace("/", "")
}
