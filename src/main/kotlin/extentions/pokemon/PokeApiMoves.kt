package extentions.pokemon

import network.model.MoveDetail
import network.model.PokeApiPokemon
import network.model.PokemonMoves

// TODO: 22/12/21 - Understand how to handle moves.
/**
 * Returns a list containing the moves of this Pokémon.
 */
private fun PokeApiPokemon.getMoves(): List<PokemonMoves> {
    val list = mutableListOf<PokemonMoves>()
    moves?.forEach { moveList ->

        val moveDetailList = mutableListOf<MoveDetail>()
        moveList.version_group_details?.forEach {
            val moveDetail = MoveDetail(
                levelLearned = it.level_learned_at,
                moveLearnMethod = it.move_learn_method?.name,
                versionGroup = it.version_group?.name
            )
            moveDetailList.add(moveDetail)
        }

        val pokemonMoves = PokemonMoves(
            move = moveList.move?.name, moveDetail = moveDetailList
        )

        list.add(pokemonMoves)
    }
    return list
}