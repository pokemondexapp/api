package extentions.pokemon

import extentions.replaceUrl
import network.PokeApiService
import network.model.*
import utils.Constants
import utils.localize

/**
 * Returns a list containing the evolution chain for this Pokémon.
 */
suspend fun PokeApiEvolutionChain.getEvolutionList(
    pokeApiService: PokeApiService, country: String
): List<PokemonEvolutions> {
    val list = mutableListOf<PokemonEvolutions>()
    chain?.getEvolutionDetail(1, pokeApiService, country)?.let { list.add(it) }
    chain?.evolves_to?.map { chain_1 ->
        list.add(chain_1.getEvolutionDetail(2, pokeApiService, country))
        chain_1.evolves_to?.map { chain_2 ->
            list.add(chain_2.getEvolutionDetail(3, pokeApiService, country))
        }
    }
    return list
}

private suspend fun Chain.getEvolutionDetail(
    stadium: Int, pokeApiService: PokeApiService, country: String
): PokemonEvolutions {

    val evolution = PokemonEvolutions()
    val id = this.species?.url?.replaceUrl("pokemon-species") ?: "0"

    val pokemon = pokeApiService.getPokemon(id)
    val species = pokeApiService.getPokemonSpecies(id)

    val type1 = if ((pokemon.types?.size ?: 0) > 0) {
        val name = pokemon.types?.get(0)?.type?.name
        pokeApiService.getTypeLocal(name ?: "0", country)
    } else null

    val type2 = if ((pokemon.types?.size ?: 0) > 1) {
        val name = pokemon.types?.get(1)?.type?.name
        pokeApiService.getTypeLocal(name ?: "0", country)
    } else null

    evolution.id = id.toInt()
    evolution.stadium = stadium
    evolution.number = pokemon.getNumber()
    evolution.name = pokemon.name?.getFormName(species, country)?.trim()
    evolution.sprite =
        "${Constants.sprite}/pokemon/other/official-artwork/${pokemon.id}.png"
    evolution.type1 = type1?.name
    evolution.type2 = type2?.name

    evolution_details?.map { detail ->
        var trigger: String = detail.trigger?.name?.localize(country) ?: ""

        if (detail.min_level != null) {
            trigger += " ${detail.min_level} "
        }

        detail.item?.name?.let { itemId ->
            evolution.itemImage = "${Constants.sprite}/items/${itemId}.png"
            evolution.trigger = when (id.toInt()) {
                25 -> "happiness".localize(country)
                26 -> itemId.localize(country)
                134 -> itemId.localize(country)
                135 -> itemId.localize(country)
                136 -> itemId.localize(country)
                196 -> "happiness-day".localize(country)
                197 -> "happiness-night".localize(country)
                470 -> itemId.localize(country)
                471 -> itemId.localize(country)
                700 -> "level-affection-fairy".localize(country)
                else -> trigger.trim()
            }
        }

    }
    return evolution
}

private fun getPokemonType(
    pokemon: PokeApiPokemon, index: Int, country: String, pokeApiService: PokeApiService
): PokemonType? {
    return if ((pokemon.types?.size ?: 0) > index) {
        val name = pokemon.types?.get(index)?.type?.name
        pokeApiService.getTypeLocal(name ?: "0", country)
    } else null
}