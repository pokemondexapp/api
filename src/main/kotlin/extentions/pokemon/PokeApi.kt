package extentions.pokemon

import extentions.replaceUrl
import network.PokeApiService
import network.model.*
import utils.Languages
import utils.MapLocalize

/**
 * Returns the number for this Pokémon.
 */
fun PokeApiPokemon.getNumber(): String {
    return when {
        (id ?: 0) < 10 -> "00${id}"
        (id ?: 0) < 100 -> "0${id}"
        else -> "$id"
    }
}

/**
 * Returns a keys values containing the statistics of this Pokémon.
 */
fun PokeApiPokemon.getStats(): MutableMap<String, Int> {
    val map = mutableMapOf<String, Int>()
    stats?.forEach {
        val key = it.stat?.name?.replace("-", "_") ?: ""
        val value = it.base_stat?.toInt() ?: 0
        map[key] = value
    }
    return map
}

/**
 * Returns a list containing the egg groups of this Pokémon.
 * @param service used to call Egg Groups,
 * @param country  used to map localized strings
 */
suspend fun PokeApiSpecies.getEggGroups(
    service: PokeApiService, country: String
): List<String> {
    val list = mutableListOf<String>()
    egg_groups?.forEach { eggGroups ->
        eggGroups.name?.let {
            val result = service.getEggGroup(it)
            list.add(result.localize(MapLocalize.NAME, country))
        }
    }
    return list
}

/**
 * Returns a list containing the egg groups of this Pokémon.
 * @param service used to call Egg Groups,
 * @param country  used to map localized strings
 */
suspend fun PokeApiPokemon.getAbilities(
    service: PokeApiService, country: String
): List<PokemonAbilities> {
    val list = mutableListOf<PokemonAbilities>()
    abilities?.forEach { pokeApiAbility ->
        pokeApiAbility.ability?.name?.let {
            val result = service.getAbility(it)
            val pokemonAbility = PokemonAbilities(
                pokeApiAbility.is_hidden ?: false,
                result.localize(MapLocalize.NAME, country),
                result.localize(MapLocalize.DESCRIPTION, country)
            )
            val ability = when (pokeApiAbility.slot) {
                1L -> pokemonAbility
                2L -> pokemonAbility
                3L -> pokemonAbility
                else -> null
            }
            if (ability != null) {
                list.add(ability)
            }
        }
    }
    return list
}

/**
 * Returns the name localized.
 */
fun PokeApiSpecies.getName(country: String): String {
    return when (country) {
        Languages.JA.value -> names?.first { it.language?.name == country }?.name ?: ""
        Languages.FR.value -> names?.first { it.language?.name == country }?.name ?: ""
        Languages.DE.value -> names?.first { it.language?.name == country }?.name ?: ""
        Languages.ES.value -> names?.first { it.language?.name == country }?.name ?: ""
        Languages.IT.value -> names?.first { it.language?.name == country }?.name ?: ""
        Languages.EN.value -> names?.first { it.language?.name == country }?.name ?: ""
        else -> names?.first { it.language?.name == country }?.name ?: ""
    }
}

/**
 * Returns the type localized.
 */
fun PokeApiType.getName(country: String): String {
    return when (country) {
        Languages.JA.value -> names?.first { it.language?.name?.contains(country) == true }?.name ?: ""
        Languages.FR.value -> names?.first { it.language?.name?.contains(country) == true }?.name ?: ""
        Languages.DE.value -> names?.first { it.language?.name?.contains(country) == true }?.name ?: ""
        Languages.ES.value -> names?.first { it.language?.name?.contains(country) == true }?.name ?: ""
        Languages.IT.value -> names?.first { it.language?.name?.contains(country) == true }?.name ?: ""
        Languages.EN.value -> names?.first { it.language?.name?.contains(country) == true }?.name ?: ""
        else -> names?.first { it.language?.name == country }?.name ?: ""
    }
}

/**
 * Returns the name localized.
 * @param [species] model mapped by PokeApi/pokemon-species
 */
fun String.getFormName(species: PokeApiSpecies, country: String): String {
    val name: String
    when {
        this.contains("-mega") -> {
            when {
                this.contains("-mega-x") -> {
                    name = when (country) {
                        Languages.JA.value -> "メガ" + species.names?.first { it.language?.name == country }?.name + "X"
                        Languages.FR.value -> "Méga-" + species.names?.first { it.language?.name == country }?.name + " X"
                        Languages.DE.value -> "Mega-" + species.names?.first { it.language?.name == country }?.name + " X"
                        Languages.ES.value -> "Mega-" + species.names?.first { it.language?.name == country }?.name + " X"
                        Languages.IT.value -> "Mega " + species.names?.first { it.language?.name == country }?.name + " X"
                        else -> "Mega " + species.names?.first { it.language?.name == country }?.name + " X"
                    }
                }
                this.contains("-mega-y") -> {
                    name = when (country) {
                        Languages.JA.value -> "メガ" + species.names?.first { it.language?.name == country }?.name + "Y"
                        Languages.FR.value -> "Méga-" + species.names?.first { it.language?.name == country }?.name + " Y"
                        Languages.DE.value -> "Mega-" + species.names?.first { it.language?.name == country }?.name + " Y"
                        Languages.ES.value -> "Mega-" + species.names?.first { it.language?.name == country }?.name + " Y"
                        Languages.IT.value -> "Mega " + species.names?.first { it.language?.name == country }?.name + " Y"
                        else -> "Mega " + species.names?.first { it.language?.name == country }?.name + " Y"
                    }
                }
                else -> {
                    name = when (country) {
                        Languages.JA.value -> "メガ" + species.names?.first { it.language?.name == country }?.name
                        Languages.FR.value -> "Méga-" + species.names?.first { it.language?.name == country }?.name
                        Languages.DE.value -> "Mega-" + species.names?.first { it.language?.name == country }?.name
                        Languages.ES.value -> "Mega-" + species.names?.first { it.language?.name == country }?.name
                        Languages.IT.value -> "Mega " + species.names?.first { it.language?.name == country }?.name
                        else -> "Mega " + species.names?.first { it.language?.name == country }?.name
                    }
                }
            }
        }
        this.contains("-gmax") -> {
            name = species.localizeName(
                country, "キョダイマックスのすがた", "Gigamax", "Gigadynamax-", "Gigamax", "Gigamax", "Gigantamax"
            )
        }
        this.contains("-alola") -> {
            name = species.localizeName(
                country, "アローラのすがた", "d'Alola", "Alola-", "de Alola", "di Alola", "Alolan"
            )
        }
        this.contains("-galar") -> {
            name = species.localizeName(
                country, "ガラルのすがた", "de Galar", "Galar-", "de Galar", "di Galar", "Galarian"
            )
        }
        this == "deoxys-normal" -> {
            name = species.localizeForm(
                country, "ノーマルフォルム", "Forme Normale", "Normalform", "Forma normal", "Forma Normale", "Normal Form"
            )
        }
        this == "deoxys-attack" -> {
            name = species.localizeForm(
                country, "アタックフォルム", "Forme Attaque", "Angriffsform", "Forma ataque", "Forma Attacco", "Attack Form"
            )
        }
        this == "deoxys-defense" -> {
            name = species.localizeForm(
                country,
                "ディフェンスフォルム",
                "Forme Défense",
                "Verteidigungsform",
                "Forma defensa",
                "Forma Difesa",
                "Defense Form"
            )
        }
        this == "deoxys-speed" -> {
            name = species.localizeForm(
                country,
                "スピードフォルム",
                "Forme Vitesse",
                "Initiativeform",
                "Forma velocidad",
                "Forma Velocità",
                "Speed Form"
            )
        }
        this == "basculin-red-striped" -> {
            name = species.localizeForm(
                country, "あかすじのすがた", "rouge", "Rotlinig", "Forma raya roja", "Forma Linearossa", "Red Striped Form"
            )
        }
        this == "basculin-blue-striped" -> {
            name = species.localizeForm(
                country, "あおすじのすがた", "bleu", "Blaulinig", "Forma raya azul", "Forma Lineablu", "Blue Striped Form"
            )
        }
        this == "wormadam-plant" -> {
            name = species.localizeForm(
                country, "くさきのミノ", "Cape Plante", "Pflanzenumhang", "Tronco planta", "Manto Pianta", "Plant Cloak"
            )
        }
        this == "wormadam-sandy" -> {
            name = species.localizeForm(
                country, "すなちのミノ", "Cape Sable", "Sandumhang", "Tronco arena", "Manto Sabbia", "Sandy Cloak"
            )
        }
        this == "wormadam-trash" -> {
            name = species.localizeForm(
                country, "ゴミのミノ", "Cape Déchet", "Lumpenumhang", "Tronco basura", "Manto Scarti", "Trash Cloak"
            )
        }
        this == "" -> {
            name = species.localizeForm(
                country, "", "", "", "", "", ""
            )
        }
        else -> {
            name = species.localizeForm(
                country, "", "", "", "", "", ""
            )
        }
    }

    return name
}

/** Returns the Pokémon form localized for each country. */
private fun PokeApiSpecies.localizeName(
    language: String, ja: String, fr: String, de: String, es: String, it: String, en: String
): String {
    return when (language) {
        Languages.JA.value -> this.names?.first { it.language?.name == language }?.name + ja
        Languages.FR.value -> this.names?.first { it.language?.name == language }?.name + " " + fr
        Languages.DE.value -> de + this.names?.first { it.language?.name == language }?.name
        Languages.ES.value -> this.names?.first { it.language?.name == language }?.name + " " + es
        Languages.IT.value -> this.names?.first { it.language?.name == language }?.name + " " + it
        else -> en + " " + this.names?.first { it.language?.name == language }?.name
    }
}

/** Returns the Pokémon form name localized for each country. */
private fun PokeApiSpecies.localizeForm(
    language: String, ja: String, fr: String, de: String, es: String, it: String, en: String
): String {
    return when (language) {
        Languages.JA.value -> this.names?.first { it.language?.name == language }?.name + " " + ja
        Languages.FR.value -> this.names?.first { it.language?.name == language }?.name + " " + fr
        Languages.DE.value -> this.names?.first { it.language?.name == language }?.name + " " + de
        Languages.ES.value -> this.names?.first { it.language?.name == language }?.name + " " + es
        Languages.IT.value -> this.names?.first { it.language?.name == language }?.name + " " + it
        else -> this.names?.first { it.language?.name == language }?.name + " " + en
    }
}

fun PokeApiSpecies.getGeneration(): Int? {
    return generation?.url?.replaceUrl("generation")?.toInt()
}

/**
 * Returns a list containing the forms for this Pokémon.
 */
fun PokeApiSpecies.getFormList(): List<NamedAPIResource> {
    val list = mutableListOf<NamedAPIResource>()
    varieties?.forEach {
        if (it.is_default == false) {
            val resource = NamedAPIResource(name = it.pokemon?.name)
            list.add(resource)
        }
    }
    return list
}
